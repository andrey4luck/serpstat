<?php

namespace app\command;

use app\ExceptionHandler;
use app\Output;


class Parse extends Command
{

    protected $path = [];
    protected $imagesArray = [];
    protected $refsArray = [];
    protected $csvFilesPaths = [];

    /*
     * array $args
     * $args[0] - command name
     * $args[1] - given url
     */
    public function __construct($args)
    {
       $this->defineResourcePath($args[1]);
    }

    public function execute()
    {
        $this->createCSVFiles();

        // gather images and references from given url
        $this->getContentFromResource();

        // put refs and images to CSV files
        $this->putToCSV();

        // output references to the CSV files
        $this->outputCSV();

    }

    protected function defineResourcePath($url)
    {
        $protocolPattern = '/http(s)?:\/\//';

        $this->path['url'] = $url;

        if (! preg_match($protocolPattern, $url)) {
            $this->path['url'] = "http://" . $url;
        }

        // cut protocol
        if (preg_match($protocolPattern, $this->path['url'])) {
            $withoutDomain = preg_replace($protocolPattern, "", $this->path['url']);
        }

        // get domain
        $withoutDomain = preg_split('/\//', ($withoutDomain ? $withoutDomain : $this->path['url']));
        $domain = $withoutDomain[0];

        $this->path['domain'] = $domain;

    }

    /*
     * bool $flag - mark if this method has been called from report
     * class in order to provide some statistics information
     */
    public function getContentFromResource($flag = false)
    {
        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML(file_get_contents($this->path['url']));

        $xpath = new \DOMXpath($dom);

        $this->fillImagesCSV($xpath->query("//img"));
        $this->fillRefsCSV($xpath->query("//a"));

        if ($flag) {
          return $this->reportInfo();
        }
    }

    protected function fillImagesCSV($nodes)
    {
        for ($i = 0; $i < $nodes->length; $i ++) {
            $item = $nodes->item($i);
            $attrValue = $item->getAttribute("src");

            array_push($this->imagesArray, $attrValue);
        }
    }

    protected function fillRefsCSV($nodes)
    {
        // get only internal references
        for ($i = 0; $i < $nodes->length; $i ++) {
            $item = $nodes->item($i);
            $attrValue = $item->getAttribute("href");

            $domainPattern = "/" . $this->path['domain'] . "/";

            if (preg_match($domainPattern, $attrValue)) {
                array_push($this->refsArray, $attrValue);
            }
        }
    }

    protected function createCSVFiles()
    {
        // create a file based at a domain name
        try {
            if (!touch(dirname(__DIR__)
                    . "/../src/" . $this->path['domain'] . "[images].csv")
                || !touch(dirname(__DIR__)
                    . "/../src/" . $this->path['domain'] . "[refs].csv")) {
                throw new \Exception("CSV file hasn't been created.");
            }
        } catch (\Exception $e) {
            ExceptionHandler::outputWarning($e->getMessage());
        }

        $fileImagesPath = dirname(__DIR__) . "/../src/" . $this->path['domain'] . "[images].csv";
        $fileRefsPath = dirname(__DIR__) . "/../src/" . $this->path['domain'] . "[refs].csv";

        $this->csvFilesPaths['Images'] = $fileImagesPath;
        $this->csvFilesPaths['Refs'] = $fileRefsPath;
    }

    protected function putToCSV()
    {
        $stuff = ['Images', 'Refs'];
        foreach ($stuff as $kind) {
            $this->putContentToCSV($kind);
        }
    }

    protected function putContentToCSV($kind)
    {
        $file = fopen($this->csvFilesPaths[$kind], 'w');

        $arrayKind = lcfirst($kind) . "Array";

        $uniqueValues = array_unique($this->$arrayKind);

        foreach ($uniqueValues as $line) {
            fputcsv($file, explode('\n', $line));
        }

        fclose($file);

    }

    protected function outputCSV()
    {
       Output::outputRefsToCSV($this->csvFilesPaths);
    }

    protected function reportInfo()
    {
        $report['images'] = count(array_unique($this->imagesArray));
        $report['refs'] = count(array_unique($this->refsArray));

        return $report;
    }

}
