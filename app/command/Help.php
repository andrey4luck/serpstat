<?php

namespace app\command;

use app\command\Command;
use app\Output;

class Help extends Command
{

    public function execute()
    {
        Output::outputHelp();
    }
}