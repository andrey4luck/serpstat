<?php

namespace app\command;

use app\Output;
use app\command\Parse;


class Report extends Command
{
    protected $path;

    public function __construct($args)
    {
        $this->path['url'] = $args[1];
    }

    public function execute()
    {
        $args = ['', $this->path['url']];

        $parser = new Parse($args);
        $reportInfo = $parser->getContentFromResource(true);

        Output::outputReport($reportInfo);
    }

}