<?php

namespace app\command;

abstract class Command
{
//    public function __construct($args)
//    {
//    }

    abstract public function execute();

}