<?php

namespace app;

use app\Output;


class ExceptionHandler
{

    public static function outputWarning(string $message)
    {
        Output::warningNotification($message);
        exit();
    }

    public static function outputInfo(string $message)
    {
        Output::infoNotification($message);
        exit();
    }

}