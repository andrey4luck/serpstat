<?php

namespace app;


class Output
{

    // colors constants
    const NC = "\033[0m";
    const YELLOW = "\033[1;33m";
    const GREEN = "\033[0;32m";
    const LB = "\033[1;34m";

    public static function warningNotification($message)
    {
        echo self::YELLOW . $message . self::NC;
    }

    public static function infoNotification($message)
    {
        echo self::YELLOW . $message . self::NC;
    }

    public static function outputRefsToCSV($paths)
    {
        foreach ($paths as $k => $v) {
            print $k . " catalog: \n";
            print $v;
            print "\n ---- \n";
        }
    }

    public static function outputHelp()
    {
        echo "`help` - outputs all available commands. \n";
        echo "`parse` <url> - Parsing the given reference and returns a csv file with all the found links on the page. \n";
        echo "`report` <domain> - outputs result analyzes for the given domain. \n";
    }

    /*
     * array $reportInformation
     * $reportInformation['images'] - amount of founded images on the page
     * $reportInformation['refs'] - amount of founded internal unique links on the page
     */
    public static function outputReport($reportInformation)
    {
        print "On the given pages founded: \n";
        print "Images - ${reportInformation['images']} \n";
        print "Internal links - ${reportInformation['refs']} \n";
        print " ------ \n ";

    }

}