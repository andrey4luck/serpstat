<?php

namespace app;

use app\command\Command;
use app\ExceptionHandler;
use app\Output;

class Controller
{

    protected $commands = ['parse', 'report', 'help'];
    protected $args = [];

    public function __construct(array $args)
    {
        $this->args = $args;
    }

    public function run()
    {
        $this->checkArgs();
        $this->redirectCommand();
    }

    public function checkArgs()
    {
        $args = $this->args;

        try {
            if (!is_array($args)) {
                throw new \Exception("Data you passed
              to the script is wrong. Insert correct data please.");
            }
        } catch (\Exception $e) {
            ExceptionHandler::outputInfo($e->getMessage());
        }

        // removing name of the script from args array
        array_shift($args);

        try {
            if (!count($args)) {
                throw new \Exception("At least one argument should be passed");
            }
        } catch (\Exception $e) {
            ExceptionHandler::outputWarning($e->getMessage());
        }

        try {
            if (!in_array($args[0], $this->commands)) {
                throw new \Exception("The command is undefined.
             Use `help` command to get all the available commands.");
            }
        } catch (\Exception $e) {
            ExceptionHandler::outputWarning($e->getMessage());
        }

        // set params
        $this->args = $args;
    }

    public function redirectCommand()
    {
        array_map(function($command) {
           if ($this->args[0] === $command) {
               $className = ucfirst($this->args[0]);
               $classTemplate = "app\command\\${className}";

               $command = new $classTemplate($this->args);
               $command->execute();
           }
        }, $this->commands);
    }

}
