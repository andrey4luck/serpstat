# README #

Script works in a such way:
* enter to the test task folder;
* run the script:
``` bash
php parser.php <command_name> <options>
```


### Available commands ###

* help - outputs all the available commands;
* parse <address> - parses the given address; saves all images src and internal links href values into a csv files; also outputs the paths for these files;
* report <address> - outputs amount of the founded unique img sources and the same value as for internal links.