#!/usr/bin/php -q
<?php


require_once 'vendor/autoload.php';
require_once 'app/Controller.php';

$app = new app\Controller($argv);
$app->run();
